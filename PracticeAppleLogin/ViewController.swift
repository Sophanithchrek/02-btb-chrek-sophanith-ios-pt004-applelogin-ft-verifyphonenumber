//
//  ViewController.swift
//  PracticeAppleLogin
//
//  Created by SOPHANITH CHREK on 26/11/20.
//

import UIKit
import AuthenticationServices

class ViewController: UIViewController {

    let appleProvider = AppleSignInClient()
    let appleBtn = ASAuthorizationAppleIDButton()
    
    @IBOutlet weak var btnSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @objc func signInWithApple(sender: ASAuthorizationAppleIDButton) {
        appleProvider.handleAppleIdRequest(block: { fullName, email, token in
        // receive data in login class.
        })
    }

    @IBAction func btnSignInPressed(_ sender: Any) {
        appleBtn.addTarget(self, action: #selector(signInWithApple(sender: )), for: .touchUpInside)
        appleBtn.sendActions(for: .touchUpInside)
    }
}

