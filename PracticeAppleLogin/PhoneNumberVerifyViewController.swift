//
//  AppleSignInClient.swift
//  PracticeAppleLogin
//
//  Created by SOPHANITH CHREK on 27/11/20.
//

import UIKit
import FirebaseCore
import FirebaseAuth

class PhoneNumberVerifyViewController: UIViewController {

    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtCodeToVerify: UITextField!
    
    var verification_id: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtCodeToVerify.isHidden = true
    }
    
    @IBAction func btnSubmit(_ sender: Any) {
        if (txtCodeToVerify.isHidden){
            if !txtPhoneNumber!.text!.isEmpty{
                Auth.auth().settings?.isAppVerificationDisabledForTesting = true
                
                PhoneAuthProvider.provider().verifyPhoneNumber(txtPhoneNumber.text!, uiDelegate: nil, completion: {verifiationID, error in
                    if(error != nil) {
                        return
                    } else{
                        self.verification_id = verifiationID
                        self.txtCodeToVerify.isHidden = false
                    }
                })
            } else {
                print("Please enter your phone number")
            }
        } else {
            if verification_id != nil{
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verification_id!, verificationCode: txtCodeToVerify.text!)
                Auth.auth().signIn(with: credential, completion: {authData, error in
                    if(error != nil) {
                        print(error.debugDescription)
                    } else{
                        print("Successfully authenticated with " + (authData?.user.phoneNumber ?? "None Phone"))
                    }
                })
            } else {
                print("Error getting verification ID")
            }
        }
    }
}
